package in.mukund


type Glass = Int
type State = Vector[Int]


class Pouring(full: State):

  enum Moves:
    case Empty(glass: Glass)      // pour out all contents of glass 
    case Fill(glass: Glass)       // Fill glass to the top
    case Pour(from: Glass, to: Glass)

    
    def apply(state: State): State = this match 
      case Empty(glass)  => state.updated(glass, 0)
      case Fill(glass)   => state.updated(glass, full(glass))
      case Pour(from, to)   =>
        val amount = (full(to) - state(to)) min state(from)
        state
          .updated(from, state(from) - amount)
          .updated(to, state(to) + amount)

  end Moves


  val moves = 
    val glasses: Range = 0 until full.length
    (for g <- glasses yield Moves.Empty(g))
    ++ (for g <- glasses yield Moves.Fill(g))
    ++ (for from <- glasses; to <- glasses; if from != to yield Moves.Pour(from, to))

  class Path(history: List[Moves], val currentState: State):
    def extend(move: Moves) = Path(move :: history, move(currentState))
    override def toString = s"${history.reverse.mkString(" ")} --> $currentState"

  val empty: State = full.map(x => 0)
  val start = Path(List(), empty)

  def pathsFrom(paths: List[Path], explored: Set[State]): LazyList[List[Path]] =
    val frontier = 
      for 
        path <- paths
        move <- moves
        next = path.extend(move)
        if !explored.contains(next.currentState)
      yield next
    paths #:: pathsFrom(frontier, explored ++ frontier.map(_.currentState))


  def solution(target: Int): LazyList[Path] =
    for 
      paths <- pathsFrom(List(start), Set(empty))
      path <- paths
      if path.currentState.contains(target)
    yield path