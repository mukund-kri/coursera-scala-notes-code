package in.mukund


import UnderstandingLaziness._

object Main extends App:
  
  // if isPrime(24) then
  //   println("23 is prime")

  time { println((1000 to 10000).filter(isPrime)(1)) }

  def nthPrime(from: Int, to: Int, n: Int): Int =
    if from > to then throw Error("No Prime")

    if isPrime(from) then
      if n == 1 then from else nthPrime(from + 1, to, n - 1)
    else nthPrime(from + 1, to, n)

  def secondPrime(from: Int, to: Int) = nthPrime(from, to, 2)

  time { println(secondPrime(1000, 10000)) }

  // Lets try that with lazy lists
  time {
    LazyList.range(1000, 10000).filter(isPrime)(1)
  }

  def time[R](block: => R): R = 
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) /  1000 + "ms")
    result
