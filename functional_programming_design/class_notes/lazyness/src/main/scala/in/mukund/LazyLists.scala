package in.mukund


object UnderstandingLaziness:
  /**
   * Checks if a given number is a prime. This is using the 6K+-1 method.
   */

  def isPrime(n: Int): Boolean = 
    if n <= 3 then return n > 1
    if n % 2 == 0 || n % 3 == 0 then return false

    var i = 5
    while i * i <= n 
    do
      if n % i == 0 || (n % (i + 2) == 0) then return false
      i += 6
    return true