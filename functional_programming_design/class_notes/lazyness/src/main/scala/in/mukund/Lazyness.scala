package in.mukund 


@main def laziness() =

  val a = {print("a "); 1}
  def b = {print("b "); 2}
  lazy val c = {print("c "); 3}

  b + c + a + c + b